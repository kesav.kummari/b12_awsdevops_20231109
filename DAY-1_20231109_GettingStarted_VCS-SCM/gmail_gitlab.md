#### Session Video:
    https://drive.google.com/file/d/1DYkpCis5C15rKyfT8cW-iOqDk-NZl-aZ/view?usp=sharing

#### Getting Started with VCS or SCM 

1. Gmail Account creation


2. GitLab Account creation
    https://about.gitlab.com/

3. Create a Repository & Grant or Add User to the Repository


```
Creating a Gmail account is a straightforward process that you can do on your computer or mobile device. Here's how to create a new Gmail account step-by-step:

On a Computer:
Go to the Gmail sign-up page.

Open your web browser and go to the Google Account creation page at https://accounts.google.com/SignUp.
Fill out the sign-up form.

Enter your first and last name.
Choose your username for your email. This will be your new email address ([username]@gmail.com).
Create a password and confirm it. Make sure it's strong and secure.
Click Next.

You may need to provide a phone number for account verification. This is also used for account recovery and security.
You can also enter a recovery email address, which is optional but recommended.
Fill out your birthdate and gender.
Agree to the privacy and terms.

Read through Google's Privacy and Terms and click “I agree” to continue.
Verify your phone number (if prompted).

If Google asks for phone verification, you’ll receive a text message with a code. Enter this code to continue.
Complete the setup.

You might be prompted to complete a few additional steps, such as adding a profile photo or setting up a recovery email.
Congratulations, you now have a Gmail account!

On a Mobile Device:
Open the Gmail app or Settings.

If you're on an Android device, you can create an account through the Settings app under "Accounts" or directly in the Gmail app. On iOS devices, you would generally do this via the Gmail app.
Tap on “Add account” and select “Google”.

Follow the prompts to reach the account creation screen.
Tap “Create account”.

Follow the same steps as above to fill out your information, agree to the terms, and verify your phone number if necessary.
Follow the on-screen instructions.

Complete the setup as directed by the app.
Things to Keep in Mind:
Username: If the username you want is taken, you'll need to try different variations or choose one of the suggestions provided by Google.
Password: Create a strong password that includes a mix of letters, numbers, and symbols to make your account secure.
Personal Information: Google will use your personal information in accordance with its privacy policy, so be sure to review it.
Phone Number: Adding a phone number is a security measure which can be helpful if you ever get locked out of your account.
Recovery Options: It's a good idea to provide a recovery phone number and email address so you can regain access to your account if you forget your password.
Once you have your account, you can start using Gmail for email, as well as other Google services like YouTube, Google Drive, and Google Calendar.
```

####

```
To create a repository in GitLab and add a user to it, you will need to have an account on GitLab and the necessary permissions to add users to a project. Here’s a step-by-step guide:

1. Create a Repository (Project) in GitLab:
Sign in to GitLab.

Open your web browser and go to GitLab.
Log in with your credentials.
Create a New Project.

Click on the “+” icon on the top bar or go to your dashboard and click on "New project".
Choose “Create blank project” or if you want to import the repository from somewhere else, select the appropriate option.
Fill Out Project Details.

Enter a project name and optionally a description.
Choose the visibility level of the project (Private, Internal, or Public).
Fill in any other details as required for the project.
Create Project.

Click on "Create project" to finalize the creation of your new repository.
2. Add or Grant User Access to the Repository:
Navigate to Your Project.

From the dashboard, click on your project to open it.
Go to Project Settings.

On the left sidebar, click on "Settings" and then "Members" or simply "Members" if it's directly available.
Invite a User.

In the "Members" section, you can add a user by entering their username or email address in the “Invite member” box.
Choose a role for the user. GitLab provides several roles like Guest, Reporter, Developer, Maintainer, and Owner, each with different permissions.
Optionally, you can set an expiration date for the user's access.
Add the User to the Project.

Click on the "Invite" button to add the user to the project.
The user you’ve added will receive an email invitation to join the project. Once they accept the invitation, they’ll have access to the repository with the permissions you’ve set.

Notes:
Roles and Permissions: Be careful with the permissions you grant; for instance, "Maintainer" can add other users and manage the project, while "Developer" can push code and create branches but cannot add other users.
Groups: If you have a group of users that need the same level of access, you can add the entire group to your project in a similar way.
Remember that in order to add users, your account must have permission to manage the project, typically as the project creator or a user with the Maintainer or Owner role. If you're a regular user without such permissions, you'll need to ask someone with the appropriate permissions to add users for you.
```